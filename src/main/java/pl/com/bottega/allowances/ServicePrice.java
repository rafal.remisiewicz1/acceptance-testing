package pl.com.bottega.allowances;

import java.util.StringJoiner;

public class ServicePrice {
    private final String service;
    private final double price;

    public ServicePrice(String service, double price) {
        this.service = service;
        this.price = price;
    }

    public String getService() {
        return service;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ServicePrice.class.getSimpleName() + "[", "]")
                .add("service='" + service + "'")
                .add("price=" + price)
                .toString();
    }
}
