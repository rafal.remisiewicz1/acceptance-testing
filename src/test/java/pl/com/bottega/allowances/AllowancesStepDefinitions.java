package pl.com.bottega.allowances;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.Assertions.assertThat;

public class AllowancesStepDefinitions {

    @Steps
    private CustomerClient customer;

    @Steps
    private ChargingPointClient chargingPoint;

    @Steps(shared = true)
    private ChargingSessionClient sessions;

    @Given("^(.+) can charge at charging point (.+)$")
    public void customer_can_charge_at_charging_point(String customerName, String chargingPointName) {
        customer.named(customerName);
        chargingPoint.named(chargingPointName);
        chargingPoint.addCustomer(customer);
    }

    @When("^(?:.+) starts? charging$")
    public void customer_starts_charging() {
        customer.startCharging(chargingPoint);
    }

    @Then("^(?:.+) should be able to start charging his car at charging point$")
    public void adam_should_be_able_to_start_charging_his_car_at_charging_point() {
        sessions.hasPendingSessionFor(customer);
    }
}
