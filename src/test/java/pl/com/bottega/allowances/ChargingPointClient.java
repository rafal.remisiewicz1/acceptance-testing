package pl.com.bottega.allowances;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.assertj.core.api.Assertions;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ChargingPointClient {

    private String chargingPointName;
    private List<CustomerClient> authorizedCustomers = new ArrayList<>();

    @Steps(shared = true)
    private ChargingSessionClient sessions;

    @Step("Adds customer {0} to chargingPoint #chargingPointName")
    public void addCustomer(CustomerClient customer) {
        authorizedCustomers.add(customer);
    }

    public void named(String name) {
        chargingPointName = name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ChargingPointClient.class.getSimpleName() + "[", "]")
                .add("chargingPointName='" + chargingPointName + "'")
                .toString();
    }
}
