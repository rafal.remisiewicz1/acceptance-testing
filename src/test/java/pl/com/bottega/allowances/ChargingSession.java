package pl.com.bottega.allowances;

import java.util.Objects;

class ChargingSession {
    private CustomerClient customer;

    public ChargingSession(CustomerClient customer) {

        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChargingSession that = (ChargingSession) o;
        return Objects.equals(customer, that.customer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer);
    }
}
