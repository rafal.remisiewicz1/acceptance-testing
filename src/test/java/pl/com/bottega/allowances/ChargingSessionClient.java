package pl.com.bottega.allowances;

import net.thucydides.core.annotations.Step;
import org.assertj.core.api.Assertions;

import java.util.ArrayList;
import java.util.List;

public class ChargingSessionClient {

    private List<ChargingSession> pendingSessions = new ArrayList<>();

    public List<ChargingSession> getPendingSessions() {
        return pendingSessions;
    }

    @Step("Starts session for {0}")
    public void startSession(CustomerClient customer) {
        pendingSessions.add(new ChargingSession(customer));
    }

    public void hasPendingSessionFor(CustomerClient customer) {
        Assertions.assertThat(pendingSessions).contains(new ChargingSession(customer));
    }
}
