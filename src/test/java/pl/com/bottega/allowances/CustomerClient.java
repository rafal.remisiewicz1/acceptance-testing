package pl.com.bottega.allowances;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;

import java.util.StringJoiner;

public class CustomerClient {

    private String actor;

    @Steps(shared = true)
    ChargingSessionClient sessions;

    public void named(String customerName) {
        actor = customerName;
    }

    @Step("#actor starts charging at {0}")
    public void startCharging(ChargingPointClient chargingPoint) {
        sessions.startSession(this);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CustomerClient.class.getSimpleName() + "[", "]")
                .add("actor='" + actor + "'")
                .toString();
    }
}
