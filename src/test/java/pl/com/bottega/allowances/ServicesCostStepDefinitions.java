package pl.com.bottega.allowances;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.List;
import java.util.Map;

public class ServicesCostStepDefinitions {

    @Given("^Prices of services:$")
    public void pricesOfServices(List<ServicePrice> data) {
        System.out.println("Data table: " + data);
    }

    @When("^Adam used (.+)$")
    public void adamUsedChargingParking(List<String> usedServices) {
        System.out.println("usedServices " + usedServices);
    }

    @Then("He should be charge (.+)")
    public void heShouldBeCharge(double total) {

    }
}
