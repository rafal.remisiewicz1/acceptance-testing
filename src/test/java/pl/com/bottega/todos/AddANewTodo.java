package pl.com.bottega.todos;

import net.serenitybdd.core.steps.Instrumented;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

class AddANewTodo implements Task {

    private Target NEW_TODO_FIELD = Target.the("New todo field").locatedBy(".new-todo");

    private final String task;

    AddANewTodo(String task) {
        this.task = task;
    }

    public static AddANewTodo called(String task) {
        return Instrumented.instanceOf(AddANewTodo.class).withProperties(task);
    }

    @Override
    @Step("{0} adds new taks #task")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(task).into(NEW_TODO_FIELD).thenHit(Keys.ENTER));
    }
}
