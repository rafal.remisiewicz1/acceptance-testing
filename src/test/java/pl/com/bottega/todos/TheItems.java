package pl.com.bottega.todos;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;
import org.yecht.Data;

import java.util.List;

class TheItems {

    private static final Target ITEMS = Target.the("Displayd items").locatedBy(".todo-list li");

    public static <T> Question<List<String>> displayed() {
        return actor -> Text.of(ITEMS).viewedBy(actor).asList();
    }
}
