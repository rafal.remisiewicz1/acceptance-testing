package pl.com.bottega.todos;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://todomvc.com/examples/angularjs/#/")
public class TodoApplication extends PageObject {
}
