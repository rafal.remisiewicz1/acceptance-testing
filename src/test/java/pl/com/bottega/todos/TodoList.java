package pl.com.bottega.todos;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.assertj.core.api.Assertions;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("http://todomvc.com/examples/angularjs/#/")
public class TodoList extends PageObject {

    @FindBy(css = ".new-todo")
    WebElementFacade newTodoField;

    public void contains(String task) {
        List<String> todos = findAll(".todo-list li").stream()
                .map(WebElementFacade::getText)
                .collect(Collectors.toList());
        Assertions.assertThat(todos).contains(task);
    }

    public void addTask(String task) {
        newTodoField.sendKeys(task, Keys.ENTER);
    }
}
