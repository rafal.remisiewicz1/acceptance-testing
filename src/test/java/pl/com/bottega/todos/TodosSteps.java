package pl.com.bottega.todos;

import net.thucydides.core.annotations.Step;

public class TodosSteps {

    TodoList todoList;

    @Step("{0} task should be added to todos list")
    public void taskShouldBeAdded(String task) {
        todoList.contains(task);
    }

    public void addANewTask(String task) {
        todoList.addTask(task);
    }

    public void openTheTodosList() {
        todoList.open();
    }
}
