package pl.com.bottega.todos;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.givenThat;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.GivenWhenThen.then;
import static net.serenitybdd.screenplay.GivenWhenThen.when;
import static org.hamcrest.Matchers.hasItem;

@RunWith(SerenityRunner.class)
public class TodosTest {

    @Managed
    WebDriver hisBrowser;

    @Steps
    TodosSteps steps;

    Actor adam = Actor.named("Adam");

    @Before
    public void setUp() throws Exception {
        givenThat(adam).can(BrowseTheWeb.with(hisBrowser));
    }

    @Test
    public void shouldAddANewTask() {
        //given
        steps.openTheTodosList();
        //when
        steps.addANewTask("Write more tests");
        //then
        steps.taskShouldBeAdded("Write more tests");
    }

    @Test
    public void shouldAddNewTaskWithScreenplaypattern() {
        givenThat(adam).attemptsTo(Open.browserOn(new TodoApplication()));
        when(adam).attemptsTo(AddANewTodo.called("Write more tests"));
        then(adam).should(seeThat(
                "the displayed items", TheItems.displayed(), hasItem("Write more tests")
        ));
    }
}
