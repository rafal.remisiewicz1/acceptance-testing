Feature: Add customer

  Scenario: Should add customer
    Given Open page 'chargingpointoperators/1/customers'
    When Type "Adam" in the input field
    And Click on enter
    And Type "Ewa" in the input field
    And Click on enter
    And Click on the Authorized button
    Then The item list size should be equal 2