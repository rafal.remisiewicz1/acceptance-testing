Feature: Allow customer to charge at my charging point

  In order to allow customers to charge at my charging point
  As a charging point operator
  I want to be able to add customer to authorized list

  Scenario: Add user to authorized list
    Given Jan has an empty authorized list
    When He adds Adam to his authorized list
    Then His authorized list should contain Adam