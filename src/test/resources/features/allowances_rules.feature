Feature: Allowances rules

  In order to protect my charging point from unauthorized customers
  As a charging point operator
  I want to be able to define allowances rules based on which customers can charge or not at my points

  Scenario: Customer is allowed to charge at charging point
    Given Adam can charge at charging point CP-001
    When Adam starts charging
    Then Adam should be able to start charging his car at charging point