Feature: Price calculator

  In order to calculate price for charging
  As a charging point operator
  I want to be able to calculate price based on charging type and time

#  Background:
#    Given Adam starts charging session

  Scenario: Fast charging
#    Given Adam starts charging session
    When Charging session takes 12 minutes
    Then Adam should pay 100.00

  Scenario: Normal charging
#    Given Adam starts charging session
    When Charging session takes 6 hours
    Then Adam should pay 80.00

  Scenario Outline: Cost calculation based on charging type and time
    Given Adam starts charging session
    And Charging type is <ChargingType> charging
    When Charging session takes <Time> minutes
    Then Adam should pay <Cost>
    Examples:
      | ChargingType | Time | Cost
      | Fast         | 12   | 20.00
      | Normal       | 360  | 80.00

  Scenario: Customer should be charged for used services
    Given Prices of services:
      | Service          | Price  |
      | Charging         | 100.00 |
      | Parking          | 5.00   |
      | Cancellation fee | 2.00   |
    When Adam used Charging, Parking
    Then He should be charge 105.00